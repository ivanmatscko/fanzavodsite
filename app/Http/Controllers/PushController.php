<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Push;

class PushController extends Controller
{
    public function push_save(Request $request){

        if(!empty($request->route('token'))){

            $Push_get = Push::where('token', $request->route('token'))->get();
            if ( empty($Push_get[0]->token) ) {

                Push::insert(array(
                    'push_date' => date("Y-m-d H:i:s"),
                    'token'=> $request->route('token'),
                    'host' => $request->route('host'),
                    'lang' => $request->route('lang'),
                    'windows' => $request->route('windows'),
                    'browser' => $request->route('browser'),
                    'country' => $request->route('country'),
                    'region' => $request->route('region'),
                    'ip' => $request->route('ip')
                ));
            }
        }

        echo  date("Y-m-d H:i:s").'<hr>';
        echo  $request->route('token').'<hr>';
        echo  $request->route('host').'<hr>';
        echo  $request->route('lang').'<hr>';
        echo  $request->route('windows').'<hr>';
        echo  $request->route('browser').'<hr>';
        echo  $request->route('country').'<hr>';
        echo  $request->route('ip').'<hr>';
    }


}
