<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Push extends Model
{
    protected $fillable = [
        'host',
        'token',
        'lang',
        'windows',
        'browser',
        'country',
        'region',
        'ip',
        'push_date'
    ];
}
