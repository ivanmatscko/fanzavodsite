<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pushes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('host')->nullable();
            $table->string('token')->nullable();
            $table->string('lang')->nullable();
            $table->string('windows')->nullable();
            $table->string('browser')->nullable();
            $table->string('country')->nullable();
            $table->string('region')->nullable();
            $table->string('ip')->nullable();
            $table->dateTime('push_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pushes');
    }
}
