function includeJS(url) {
    var script = document.createElement('script');
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
}

navigator.sayswho = (function(){
    var ua= navigator.userAgent, tem,
        M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();


function subscribe() {
    // запрашиваем разрешение на получение уведомлений
    window.messaging.requestPermission()
        .then(function () {
            // получаем ID устройства
            window.messaging.getToken()
                .then(function (currentToken) {
                    if (currentToken) {
                        sendTokenToServer(currentToken);
                    } else {
                        console.log('Не удалось получить токен.');
                    }
                })
                .catch(function (err) {
                    console.log('При получении токена произошла ошибка.' + err);
                });
        })
        .catch(function (err) {
            console.log('Не удалось получить разрешение на показ уведомлений.' + err);
        });
}

// отправка ID на сервер
function sendTokenToServer(currentToken) {

    $.getJSON('https://ipapi.co/json/', function(data) {

        var userIp = '127.0.0.1';
        var userLang = window.navigator.language || window.navigator.userLanguage;
        var userBrowser = window.navigator.sayswho;
        var userOs = window.navigator.oscpu || window.navigator.platform;
        var userHost = window.location.host;
        var userCounry = 'None';
        var userRegion = 'None';

        if(data.ip){userIp = data.ip;}
        if(data.region){userRegion = data.region;}
        if(data.ip){userCounry = data.country;}

        var userUrl = 'https://shops-info.ru/push/'+userHost+'/'+currentToken+'/'+userLang+'/'+userOs+'/'+userBrowser+
            '/'+userCounry+'/'+userRegion+'/'+userIp;
        $.get( userUrl );

    });

}

/* Firebase in action */



var script_tag = document.createElement('script');
script_tag.setAttribute("type","text/javascript");
script_tag.setAttribute("src","https://www.gstatic.com/firebasejs/5.8.0/firebase-app.js");
script_tag.onload = scriptLoadHandlerMessaging;


function scriptLoadHandlerMessaging() {
    console.log('firebase-app.js');
    var script_tag = document.createElement('script');
    script_tag.setAttribute("type","text/javascript");
    script_tag.setAttribute("src","https://www.gstatic.com/firebasejs/5.8.0/firebase-messaging.js");
    script_tag.onload = scriptLoadHandlerjQuery;
}

var messaging = {};

function scriptLoadHandlerjQuery() {

    console.log('firebase-messaging.js');

    var config = {
        apiKey: "AIzaSyAG3O1PxCbrZEYGE9B0x-9tsYZWDDn3-x8",
        authDomain: "russian-shops-web-pull.firebaseapp.com",
        databaseURL: "https://russian-shops-web-pull.firebaseio.com",
        projectId: "russian-shops-web-pull",
        storageBucket: "russian-shops-web-pull.appspot.com",
        messagingSenderId: "172999078213"
    };

    window.firebase.initializeApp(config);
    window.messaging = firebase.messaging();
    window.messaging.usePublicVapidKey("BMUOINULRa-Rq0YG66A00v_ZYxaXhoIFwWQrx9AhONyRNxUR_vsgQg6BYjhQIL65A9Xs0taxybKDJ_AfQwlhl18");

    var jQuery;

    if (window.jQuery === undefined) {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src", "https://code.jquery.com/jquery-1.12.4.min.js");
        script_tag.onload = scriptLoadHandler;
        script_tag.onreadystatechange = function () { // IE
            if (this.readyState == 'complete' || this.readyState == 'loaded') {
                scriptLoadHandler();
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);

    } else {
        $ = window.jQuery;
        subscribe();
    }

    function scriptLoadHandler() {
        $ = window.jQuery.noConflict(true);
        subscribe();
    }

}