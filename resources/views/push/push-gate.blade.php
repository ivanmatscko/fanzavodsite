<!DOCTYPE html>
<html>
<head>
    <title>Push</title>
</head>
<body>
<!-- Firebase App is always required and must be first -->
<script src="https://www.gstatic.com/firebasejs/5.8.0/firebase-app.js"></script>
<!-- Add additional services that you want to use -->
<script src="https://www.gstatic.com/firebasejs/5.8.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.0/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.0/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.0/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.8.0/firebase-functions.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script type="text/javascript">

    navigator.sayswho = (function(){
        var ua= navigator.userAgent, tem,
            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
            if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();


    var config = {
        apiKey: "AIzaSyAG3O1PxCbrZEYGE9B0x-9tsYZWDDn3-x8",
        authDomain: "russian-shops-web-pull.firebaseapp.com",
        databaseURL: "https://russian-shops-web-pull.firebaseio.com",
        projectId: "russian-shops-web-pull",
        storageBucket: "russian-shops-web-pull.appspot.com",
        messagingSenderId: "172999078213"
    };

    firebase.initializeApp(config);

    var messaging = firebase.messaging();

    messaging.usePublicVapidKey("BMUOINULRa-Rq0YG66A00v_ZYxaXhoIFwWQrx9AhONyRNxUR_vsgQg6BYjhQIL65A9Xs0taxybKDJ_AfQwlhl18");

    function subscribe() {
        // запрашиваем разрешение на получение уведомлений
        messaging.requestPermission()
                .then(function () {
                    // получаем ID устройства
                    messaging.getToken()
                            .then(function (currentToken) {


                                if (currentToken) {
                                    sendTokenToServer(currentToken);
                                } else {
                                   // alert('Не удалось получить токен.');
                                }
                            })
                            .catch(function (err) {
                                //alert('При получении токена произошла ошибка.' + err);
                            });
                })
                .catch(function (err) {
                    //alert('Не удалось получить разрешение на показ уведомлений.' + err);
                });
    }

    // отправка ID на сервер
    function sendTokenToServer(currentToken) {

        $.getJSON('https://ipapi.co/json/', function(data) {

            var userIp = '127.0.0.1';
            var userLang = window.navigator.language || window.navigator.userLanguage;
            var userBrowser = window.navigator.sayswho;
            var userOs = window.navigator.oscpu || window.navigator.platform;
            var userHost = window.location.host;
            var userCounry = 'None';
            var userRegion = 'None';

            if(data.ip){userIp = data.ip;}
            if(data.region){userRegion = data.region;}
            if(data.ip){userCounry = data.country;}

            var userUrl = 'https://shops-info.ru/push/'+userHost+'/'+currentToken+'/'+userLang+'/'+userOs+'/'+userBrowser+'/'+userCounry+'/'+userRegion+'/'+userIp;
            $.get( userUrl );

        });

    }

    subscribe();

</script>
</body>
</html>