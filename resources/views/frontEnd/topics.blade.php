@extends('frontEnd.layout')

@section('content')

    <section id="inner-headline">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li><a href="{{ route("Home") }}"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i>
                        </li>
                        @if(@$WebmasterSection!="none")
                            <li class="active">{!! trans('backLang.'.$WebmasterSection->name) !!}</li>
                        @elseif(@$search_word!="")
                            <li class="active">{{ @$search_word }}</li>
                        @else
                            <li class="active">{{ $User->name }}</li>
                        @endif
                        @if($CurrentCategory!="none")
                            @if(!empty($CurrentCategory))
                                <?php
                                $category_title_var = "title_" . trans('backLang.boxCode');
                                ?>
                                <li class="active"><i
                                            class="icon-angle-right"></i>{{ $CurrentCategory->$category_title_var }}
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="row">
                    @if(count($Categories)>0)
                        @include('frontEnd.includes.leftMenu')
                    @endif


                        <div class="col-lg-9">p
                            <div class="row">
                                <div class="col-sm-12">
                                    @if(count($Categories)>0)
                                        @include('frontEnd.includes.side')
                                    @endif
                                    <div></div>
                                </div>
                            </div>
                        </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                {!! $Topics->links() !!}
            </div>
            <div class="col-lg-4 text-right">
                <br>
                <small>{{ $Topics->firstItem() }} - {{ $Topics->lastItem() }} {{ trans('backLang.of') }}
                    ( {{ $Topics->total()  }} ) {{ trans('backLang.records') }}</small>
            </div>
        </div>

    </section>

@endsection