<header>
    <div class="site-top">

    </div>
    <div class="navbar navbar-default">
        <div class="logo-holder-container">
            <div class="logo-holder-row">
                <div class="col-1">
                    <a class="logo-holder" href="{{ route("Home") }}">
                        @if(Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode')) !="")
                            <img alt="" src="{{ URL::to('uploads/settings/'.Helper::GeneralSiteSettings("style_logo_" . trans('backLang.boxCode'))) }}">
                        @else
                            <img alt="" src="{{ URL::to('uploads/settings/nologo.png') }}">
                        @endif
                    </a>
                </div>
                <div class="text-center col-2">
                    <img class="logo-image" alt="" src="{{ URL::to('uploads/settings/logo-b.jpg') }}">
                </div>
                <div class="col-3">
                    <div class="viber-holder text-right">
                        <a href="viber://chat?number={{ Helper::GeneralSiteSettings("contact_t5") }}">
                            <span dir="ltr">{{ Helper::GeneralSiteSettings("contact_t5") }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            @include('frontEnd.includes.menu')
        </div>
    </div>
</header>