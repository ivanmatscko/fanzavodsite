
<div class="col-lg-3">
    <div class="menu_end_social">
        <ul>
				<?php
				$title_var = "title_" . trans('backLang.boxCode');
				$title_var2 = "title_" . trans('backLang.boxCodeOther');
				$details_var = "details_" . trans('backLang.boxCode');
				$details_var2 = "details_" . trans('backLang.boxCodeOther');
				$slug_var = "seo_url_slug_" . trans('backLang.boxCode');
				$slug_var2 = "seo_url_slug_" . trans('backLang.boxCodeOther');
				$section_url = "";
				?>
        @foreach($HomeTopics as $HomeTopic)
						<?php
						if ($HomeTopic->$title_var != "") {
								$title = $HomeTopic->$title_var;
						} else {
								$title = $HomeTopic->$title_var2;
						}
						if ($HomeTopic->$details_var != "") {
								$details = $details_var;
						} else {
								$details = $details_var2;
						}
						if ($HomeTopic->webmasterSection->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
								if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
										$section_url = url(trans('backLang.code') . "/" . $HomeTopic->webmasterSection->$slug_var);
								} else {
										$section_url = url($HomeTopic->webmasterSection->$slug_var);
								}
						} else {
								if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
										$section_url = url(trans('backLang.code') . "/" . $HomeTopic->webmasterSection->name);
								} else {
										$section_url = url($HomeTopic->webmasterSection->name);
								}
						}

						if ($HomeTopic->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
								if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
										$topic_link_url = url(trans('backLang.code') . "/" . $HomeTopic->$slug_var);
								} else {
										$topic_link_url = url($HomeTopic->$slug_var);
								}
						} else {
								if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
										$topic_link_url = route('FrontendTopicByLang', ["lang" => trans('backLang.code'), "section" => $HomeTopic->webmasterSection->name, "id" => $HomeTopic->id]);
								} else {
										$topic_link_url = route('FrontendTopic', ["section" => $HomeTopic->webmasterSection->name, "id" => $HomeTopic->id]);
								}
						}

						?>

            <!-- ########################### -->
                <li> <a href="{{ $topic_link_url }}">{{ $title }}</a></li>
            @endforeach

        </ul>
        <div class="social_wrapp">
            <p>Мы в соцсетях</p>
            <ul class="social">
                <li>
                    <a href="#">
                        <img src="<?php echo asset('frontEnd/img/images/a11.png');?>">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo asset('frontEnd/img/images/a13.png');?>">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo asset('frontEnd/img/images/a15.png');?>">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>