@extends('frontEnd.layout')

@section('content')

    <!-- start Home Slider -->
    @include('frontEnd.includes.slider')
    <!-- end Home Slider -->


    @if(count($TextBanners)>0)
        @foreach($TextBanners->slice(0,1) as $TextBanner)
            <?php
            try {
                $TextBanner_type = $TextBanner->webmasterBanner->type;
            } catch (Exception $e) {
                $TextBanner_type = 0;
            }
            ?>
        @endforeach
        <?php
        $title_var = "title_" . trans('backLang.boxCode');
        $details_var = "details_" . trans('backLang.boxCode');
        $file_var = "file_" . trans('backLang.boxCode');

        $col_width = 12;
        if (count($TextBanners) == 2) {
            $col_width = 6;
        }
        if (count($TextBanners) == 3) {
            $col_width = 4;
        }
        if (count($TextBanners) > 3) {
            $col_width = 3;
        }
        ?>
        <section class="content-row-no-bg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="margin-bottom: 0;">
                            @foreach($TextBanners as $TextBanner)
                                <div class="col-lg-{{$col_width}}">
                                    <div class="box">
                                        <div class="box-gray aligncenter">
                                            @if($TextBanner->code !="")
                                                {!! $TextBanner->code !!}
                                            @else
                                                @if($TextBanner->icon !="")
                                                    <div class="icon">
                                                        <i class="fa {{$TextBanner->icon}} fa-3x"></i>
                                                    </div>
                                                @elseif($TextBanner->$file_var !="")
                                                    <img src="{{ URL::to('uploads/banners/'.$TextBanner->$file_var) }}"
                                                         alt="{{ $TextBanner->$title_var }}"/>
                                                @endif
                                                <h4>{!! $TextBanner->$title_var !!}</h4>
                                                @if($TextBanner->$details_var !="")
                                                    <p>{!! nl2br($TextBanner->$details_var) !!}</p>
                                                @endif
                                            @endif

                                        </div>
                                        @if($TextBanner->link_url !="")
                                            <div class="box-bottom">
                                                <a href="{!! $TextBanner->link_url !!}">{{ trans('frontLang.moreDetails') }}</a>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if(count($HomeTopics)>0)
        <section class="content-row-no-bg">
            <div class="container">

                <div class="row">

                    <div class="col-lg-3">
                        <div class="menu_end_social">
                            <ul>
		                        <?php
		                        $title_var = "title_" . trans('backLang.boxCode');
		                        $title_var2 = "title_" . trans('backLang.boxCodeOther');
		                        $details_var = "details_" . trans('backLang.boxCode');
		                        $details_var2 = "details_" . trans('backLang.boxCodeOther');
		                        $slug_var = "seo_url_slug_" . trans('backLang.boxCode');
		                        $slug_var2 = "seo_url_slug_" . trans('backLang.boxCodeOther');
		                        $section_url = "";
		                        ?>
                            @foreach($HomeTopics as $HomeTopic)
				                        <?php
				                        if ($HomeTopic->$title_var != "") {
						                        $title = $HomeTopic->$title_var;
				                        } else {
						                        $title = $HomeTopic->$title_var2;
				                        }
				                        if ($HomeTopic->$details_var != "") {
						                        $details = $details_var;
				                        } else {
						                        $details = $details_var2;
				                        }
				                        if ($HomeTopic->webmasterSection->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
						                        if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
								                        $section_url = url(trans('backLang.code') . "/" . $HomeTopic->webmasterSection->$slug_var);
						                        } else {
								                        $section_url = url($HomeTopic->webmasterSection->$slug_var);
						                        }
				                        } else {
						                        if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
								                        $section_url = url(trans('backLang.code') . "/" . $HomeTopic->webmasterSection->name);
						                        } else {
								                        $section_url = url($HomeTopic->webmasterSection->name);
						                        }
				                        }

				                        if ($HomeTopic->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
						                        if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
								                        $topic_link_url = url(trans('backLang.code') . "/" . $HomeTopic->$slug_var);
						                        } else {
								                        $topic_link_url = url($HomeTopic->$slug_var);
						                        }
				                        } else {
						                        if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
								                        $topic_link_url = route('FrontendTopicByLang', ["lang" => trans('backLang.code'), "section" => $HomeTopic->webmasterSection->name, "id" => $HomeTopic->id]);
						                        } else {
								                        $topic_link_url = route('FrontendTopic', ["section" => $HomeTopic->webmasterSection->name, "id" => $HomeTopic->id]);
						                        }
				                        }

				                        ?>

                                <!-- ########################### -->
                                    <li> <a href="{{ $topic_link_url }}">{{ $title }}</a></li>
                                @endforeach

                            </ul>
                            <div class="social_wrapp">
                                <p>Мы в соцсетях</p>
                                <ul class="social">
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('frontEnd/img/images/a11.png');?>">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('frontEnd/img/images/a13.png');?>">
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src="<?php echo asset('frontEnd/img/images/a15.png');?>">
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-9">
                        <div class="big_text_block">
                            <h1 class="text_title">
                                <span class="text_title_elem_1">
                                    <img src="<?php echo asset('frontEnd/img/images/a03.png');?>">
                                </span>
                                3 по цене 2-х
                            </h1>
                            <div class="wrap_img">
                                <img src="<?php echo asset('frontEnd/img/images/a07.png');?>">
                            </div>
                            <div class="text_block">
                                <p>
                                    На этом сайте Вы найдете самую последнюю информацию о нас. Наша компания постоянно развивается и растет. Мы предлагаем широкий ассортимент товаров, созданных для  Вас и Ваших близких! Наши изделия могут украсить Ваш интерьер, или стать прекрасным подарком родителям, друзьям, коллегам.
                                </p>
                                <p>
                                    Мы изготавливаем на заказ декор любой сложности, конфигурации, цвета, размера, события и т.д. полностью индивидуальный дизайн, авторская разработка. К Вашим услугам команда профессионалов, которые учтут все Ваши пожелания и постараются воплотить самые разнообразные идеи, создать приятный, эксклюзивный подарок для Вас и Ваших близких!
                                </p>
                            </div>
                            <div class="text_block">
                                <h2>ВОЗМОЖНО ИЗГОТОВЛЕНИЕ СЛЕДУЮЩИХ ИЗДЕЛИЙ:</h2>
                                <ul>
                                    <li>
                                        - фоторамки;
                                    </li>
                                    <li>
                                        - семейный календарь, детские метрики;
                                    </li>
                                    <li>
                                        - ключницы, плечики, вешалки для легкой одежды, медальницы;
                                    </li>
                                    <li>
                                        - часы, фоны для демонстрации рукоделия, блокноты;
                                    </li>
                                    <li>
                                        - слова, буквы, цифры, символы, фигуры для фотосессий и интерьеров;
                                    </li>
                                    <li>
                                        - медали из дерева с гравировкой, елочные и новогодние украшения;
                                    </li>
                                    <li>
                                        - шкатулки, коробочки, разного рода сокровищницы;
                                    </li>
                                    <li>
                                        - флешки в деревянном корпусе с гравировками;
                                    </li>
                                    <li>
                                        - ростомеры детские настенные;
                                    </li>
                                    <li>
                                        - свадебный декор;
                                    </li>
                                    <li>
                                        - кухонные сувениры: подставки под горячее, полочки для специй,
                                        вешалки для полотенец, салфетницы, и т.д.
                                    </li>
                                </ul>

                                <h3>Будем рады воплотить все Ваши самые смелые задумки!</h3>
                                <h3>В надежде на приятное сотрудничество :) всем хорошего настроения!</h3>
                            </div>
                        </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="more-btn">
                            <a href="{{ url($section_url) }}" class="btn btn-theme"><i
                                        class="fa fa-angle-left"></i>&nbsp; {{ trans('frontLang.viewMore') }}
                                &nbsp;<i
                                        class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    @endif


    @if(count($HomePhotos)>0)
        <section class="content-row-no-bg">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="home-row-head">
                            <h2 class="heading">{{ trans('frontLang.homeContents2Title') }}</h2>
                            <small>{{ trans('frontLang.homeContents2desc') }}</small>
                        </div>
                        <div class="row">
                            <section id="projects">
                                <ul id="thumbs" class="portfolio">
                                    <?php
                                    $title_var = "title_" . trans('backLang.boxCode');
                                    $title_var2 = "title_" . trans('backLang.boxCodeOther');
                                    $details_var = "details_" . trans('backLang.boxCode');
                                    $details_var2 = "details_" . trans('backLang.boxCodeOther');
                                    $slug_var = "seo_url_slug_" . trans('backLang.boxCode');
                                    $slug_var2 = "seo_url_slug_" . trans('backLang.boxCodeOther');
                                    $section_url = "";
                                    $ph_count = 0;
                                    ?>
                                    @foreach($HomePhotos as $HomePhoto)
                                        <?php
                                        if ($HomePhoto->$title_var != "") {
                                            $title = $HomePhoto->$title_var;
                                        } else {
                                            $title = $HomePhoto->$title_var2;
                                        }
                                        if ($HomePhoto->webmasterSection->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
                                            if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                                $section_url = url(trans('backLang.code') . "/" . $HomePhoto->webmasterSection->$slug_var);
                                            } else {
                                                $section_url = url($HomePhoto->webmasterSection->$slug_var);
                                            }
                                        } else {
                                            if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                                $section_url = url(trans('backLang.code') . "/" . $HomePhoto->webmasterSection->name);
                                            } else {
                                                $section_url = url($HomePhoto->webmasterSection->name);
                                            }
                                        }
                                        ?>
                                        @foreach($HomePhoto->photos as $photo)
                                            @if($ph_count<12)
                                                <li class="col-lg-2 design" data-id="id-0" data-type="web">
                                                    <div class="relative">
                                                        <div class="item-thumbs">
                                                            <a class="hover-wrap fancybox" data-fancybox-group="gallery"
                                                               title="{{ $title }}"
                                                               href="{{ URL::to('uploads/topics/'.$photo->file) }}">
                                                                <span class="overlay-img"></span>
                                                                <span class="overlay-img-thumb"><i
                                                                            class="fa fa-search-plus"></i></span>
                                                            </a>
                                                            <!-- Thumb Image and Description -->
                                                            <img src="{{ URL::to('uploads/topics/'.$photo->file) }}"
                                                                 alt="{{ $title }}">
                                                        </div>
                                                    </div>
                                                </li>
                                            @endif
                                            <?php
                                            $ph_count++;
                                            ?>
                                        @endforeach
                                    @endforeach

                                </ul>
                            </section>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="more-btn">
                                    <a href="{{ url($section_url) }}" class="btn btn-theme"><i
                                                class="fa fa-angle-left"></i>&nbsp; {{ trans('frontLang.viewMore') }}
                                        &nbsp;<i
                                                class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif

    @if(count($HomePartners)>0)
        <section class="content-row-no-bg top-line">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="home-row-head">
                            <h2 class="heading">{{ trans('frontLang.partners') }}</h2>
                            <small>{{ trans('frontLang.partnersMsg') }}</small>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="partners_carousel slide" id="myCarousel" style="direction: ltr">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <?php
                                    $ii = 0;
                                    $title_var = "title_" . trans('backLang.boxCode');
                                    $title_var2 = "title_" . trans('backLang.boxCodeOther');
                                    $details_var = "details_" . trans('backLang.boxCode');
                                    $details_var2 = "details_" . trans('backLang.boxCodeOther');
                                    $slug_var = "seo_url_slug_" . trans('backLang.boxCode');
                                    $slug_var2 = "seo_url_slug_" . trans('backLang.boxCodeOther');
                                    $section_url = "";
                                    ?>

                                    @foreach($HomePartners as $HomePartner)
                                        <?php
                                        if ($HomePartner->$title_var != "") {
                                            $title = $HomePartner->$title_var;
                                        } else {
                                            $title = $HomePartner->$title_var2;
                                        }
                                        if ($HomePartner->webmasterSection->$slug_var != "" && Helper::GeneralWebmasterSettings("links_status")) {
                                            if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                                $section_url = url(trans('backLang.code') . "/" . $HomePartner->webmasterSection->$slug_var);
                                            } else {
                                                $section_url = url($HomePartner->webmasterSection->$slug_var);
                                            }
                                        } else {
                                            if (trans('backLang.code') != env('DEFAULT_LANGUAGE')) {
                                                $section_url = url(trans('backLang.code') . "/" . $HomePartner->webmasterSection->name);
                                            } else {
                                                $section_url = url($HomePartner->webmasterSection->name);
                                            }
                                        }

                                        if ($ii == 6) {
                                            echo "
                                                    </ul>
                                </div><!-- /Slide -->
                                <div class='item'>
                                    <ul class='thumbnails'>
                                                    ";
                                            $ii = 0;
                                        }
                                        ?>
                                        <li class="col-sm-2">
                                            <div>
                                                <div class="thumbnail">
                                                    <img src="{{ URL::to('uploads/topics/'.$HomePartner->photo_file) }}"
                                                         data-placement="bottom" title="{{ $title }}"
                                                         alt="{{ $title }}">
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                        </li>
                                        <?php
                                        $ii++;
                                        ?>
                                    @endforeach

                                </ul>
                            </div><!-- /Slide -->
                        </div>
                        <nav>
                            <ul class="control-box pager">
                                <li><a data-slide="prev" href="#myCarousel" class=""><i
                                                class="fa fa-angle-left"></i></a></li>
                                {{--<li><a href="{{ url($section_url) }}">{{ trans('frontLang.viewMore') }}</a>--}}
                                {{--</li>--}}
                                <li><a data-slide="next" href="#myCarousel" class=""><i
                                                class="fa fa-angle-right"></i></a></li>
                            </ul>
                        </nav>
                        <!-- /.control-box -->

                    </div><!-- /#myCarousel -->
                </div>

            </div>

        </section>
    @endif

@endsection